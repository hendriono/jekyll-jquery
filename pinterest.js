<script type='text/javascript'>
//jQuery required in the head of your template
 $().ready(function() {
		
	$('.entry-content img').each(function(){
		var $permalink = $(this).parents('.post-outer').find('.timestamp-link').attr('href');
		$(this).wrap('<div class="pinme" style="' + $(this).attr('style') + '" />').after('<a href="http://pinterest.com/pin/create/button/?url=' + $permalink + '&media=' + $(this).attr('src') + '&description=' + $(this).attr('alt') + '" class="pin-it-button" count-layout="horizontal">Pin It</a>');
	});
	
});

(function() {
    window.PinIt = window.PinIt || { loaded:false };
    if (window.PinIt.loaded) return;
    window.PinIt.loaded = true;
    function async_load(){
        var s = document.createElement("script");
        s.type = "text/javascript";
        s.async = true;
        s.src = "http://assets.pinterest.com/js/pinit.js";
        var x = document.getElementsByTagName("script")[0];
        x.parentNode.insertBefore(s, x);
    }
    if (window.attachEvent)
        window.attachEvent("onload", async_load);
    else
        window.addEventListener("load", async_load, false);
})();
</script>